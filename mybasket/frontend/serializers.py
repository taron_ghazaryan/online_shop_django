from rest_framework import serializers

from .models import Product, Order, Basket, Tag, ProductImages, User, Review


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        fields = [
            'pk',
            'username',
            'first_name',
            'last_name',
            'father_name',
            'email',
            'phone',
            'is_staff',
            'is_superuser',
            'is_active',
            'last_login',
            'date_joined',
            'date_of_birth',
            'avatar',
        ]


class TagSerializer(serializers.ModelSerializer):

    class Meta:

        model = Tag
        fields = [
            'pk',
            'tag'
        ]


class ProductSerializer(serializers.ModelSerializer):
    class Meta:

        model = Product
        fields = [
            'pk',
            'name',
            'description',
            'price',
            'created_at',
            'count',
            'free_delivery',
            'archived',
            'rating',
            'tags',
            'sale',
            'preview'
        ]


class ProductImagesSerializer(serializers.ModelSerializer):

    class Meta:
        model = ProductImages
        fields = [
            'pk',
            'product',
            'image'
        ]


class ReviewSerializer(serializers.ModelSerializer):

    class Meta:

        model = Review
        fields = [
            'pk',
            'user',
            'product',
            'review',
            'review_image'
        ]


class OrderSerializer(serializers.ModelSerializer):

    class Meta:

        model = Order
        fields = [
            'pk',
            'delivery_address',
            'created_at',
            'promo_code',
            'user',
            'product'
        ]


class BasketSerializer(serializers.ModelSerializer):

    class Meta:

        model = Basket
        fields = [
            'pk',
            'product',
            'quantity',
            'created_at',
            'total_price'
        ]
