from django.db import models

from django.core.validators import MinValueValidator, MaxValueValidator, ValidationError

from django.contrib.auth.models import AbstractUser

from .managers import CustomUserManager

from django.contrib.auth import get_user_model


# Create your models here.

# UserModel = get_user_model()


def avatar_upload_to(instance: "User", filename: str) -> str:
    return f"users/{instance.username}/avatar/{filename}"


class User(AbstractUser):
    email = models.EmailField("email address", unique=True, )
    father_name = models.CharField(max_length=20, blank=True, null=True)
    phone = models.CharField(max_length=20, blank=False, null=False)
    date_of_birth = models.DateField(blank=True, null=True)
    avatar = models.ImageField(null=True, upload_to=avatar_upload_to)

    username = models.CharField("username",
                                max_length=150,
                                help_text=(
                                    "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
                                ),
                                validators=[AbstractUser.username_validator],
                                error_messages={
                                    "unique": "A user with that username already exists.",
                                },
                                null=True,
                                blank=True,
                                )

    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = CustomUserManager()


class Tag(models.Model):
    tag = models.CharField(max_length=50, blank=True, null=False)

    # def __str__(self):
    #     return f"{self.tag}"


def product_prev_upload_path(instance: "Product", filename: str) -> str:
    if not instance.pk:
        temp_instance = Product.objects.create(
            name=instance.name,
            description=instance.description,
            price=instance.price,
            archived=instance.archived
        )
        instance.pk = temp_instance.pk
        temp_instance.delete()
    return f"products/{instance.pk}/preview/{filename}"


class Product(models.Model):
    name = models.CharField(max_length=50)
    description = models.CharField(max_length=200, blank=True, null=False)
    price = models.DecimalField(max_digits=6, decimal_places=2)
    created_at = models.DateField(auto_now_add=True)
    count = models.PositiveIntegerField(default=0)
    free_delivery = models.BooleanField(default=False)
    archived = models.BooleanField(default=False)
    rating = models.FloatField(default=0.0, validators=[MinValueValidator(1.0), MaxValueValidator(10.0)])
    tags = models.ManyToManyField(Tag, related_name='products')
    sale = models.SmallIntegerField(default=0)
    preview = models.ImageField(null=True, blank=True, upload_to=product_prev_upload_path)


def upload_product_image_path(instance: "ProductImages", filename: str) -> str:
    return f"products/{instance.product.pk}/images/{filename}"


def validator_image(image: 'ProductImages'):
    if not image.name.lower().endswith('jpg'):
        raise ValidationError('bad format image')


class ProductImages(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    image = models.ImageField(null=True, blank=True, validators=[validator_image], upload_to=upload_product_image_path)


def review_img_upload_path(instance: 'Review', filename: str) -> str:
    return f"products/{instance.product.pk}/review/{instance.user.pk}/{filename}"


class Review(models.Model):
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    review = models.TextField(max_length=500, null=False)
    review_image = models.ImageField(upload_to=review_img_upload_path)


class Order(models.Model):
    delivery_address = models.CharField(max_length=50)
    created_at = models.DateField(auto_now_add=True)
    promo_code = models.CharField(max_length=20)
    user = models.ForeignKey(User, on_delete=models.PROTECT)
    product = models.ManyToManyField(Product, related_name='orders')


class Basket(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    product = models.ForeignKey(Product, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    total_price = models.DecimalField(max_digits=6, decimal_places=2)

    def __str__(self):
        return f"Basket for {self.user.username} | product {self.product.name}"
