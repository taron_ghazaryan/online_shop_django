from django.contrib.auth import login, authenticate
from django.shortcuts import render, redirect
from django.views import View
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import User, Tag, Product, ProductImages, Order, Review, Basket

from rest_framework import generics, permissions, status
from rest_framework.viewsets import ModelViewSet
from rest_framework.generics import ListAPIView, CreateAPIView, RetrieveAPIView, DestroyAPIView, UpdateAPIView
from rest_framework.filters import SearchFilter, OrderingFilter
from django_filters.rest_framework import DjangoFilterBackend

from .serializers import UserSerializer, ProductSerializer, TagSerializer, ProductImagesSerializer, OrderSerializer, \
    ReviewSerializer, BasketSerializer


class SignInView(View):
    def post(self, request):
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('frontend:about')
        else:
            return render(request, 'frontend/signIn.html', {'error': 'Invalid login or password'})

    def get(self, request):
        if request.user.is_authenticated:
            return redirect('frontend:about')
        return render(request, 'frontend/signIn.html')


class UserViewSet(ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer


class TagViewSet(ModelViewSet):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
    permission_classes = [permissions.IsAuthenticated]


class CreateTagView(CreateAPIView):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer


class ProductViewSet(ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = [permissions.IsAuthenticated]


class ProductImagesViewSet(ModelViewSet):
    queryset = ProductImages.objects.all()
    serializer_class = ProductImagesSerializer


class OrderViewSet(ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_class = permissions.IsAuthenticatedOrReadOnly


class ReviewViewSet(ModelViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewSerializer


class BasketViewSet(ModelViewSet):
    queryset = Basket.objects.all()
    serializer_class = BasketSerializer


class SignInAPIView(APIView):
    def post(self, request, *args, **kwargs):
        email = request.data.get('email')
        password = request.data.get('password')

        user = authenticate(email=email , password=password)
        if user is not None:
            # Пользователь аутентифицирован
            return Response({'message': 'Успешная аутентификация'}, status=status.HTTP_200_OK)
        else:
            # Неверные учетные данные
            return Response({'message': 'Неверные учетные данные'}, status=status.HTTP_401_UNAUTHORIZED)