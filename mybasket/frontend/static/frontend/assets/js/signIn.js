var mix = {
    methods: {
        signIn () {
            const email = document.querySelector('#login').value;
            const password = document.querySelector('#password').value;

            axios.post('/api/sign-in/', {
                email: email,
                password: password
            })
            .then(response => {
                console.log(response.data);
                location.assign(`/`);
            })
            .catch(error => {
                console.error(error);
            });
        }
    },
    mounted() {
    },
    data() {
        return {}
    }
}
