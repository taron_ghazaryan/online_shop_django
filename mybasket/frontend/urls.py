from django.urls import path, include
from django.views.generic import TemplateView
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework.routers import DefaultRouter

from rest_framework_simplejwt.views import TokenRefreshView, TokenObtainPairView

from django.contrib.auth.views import LoginView

from .views import (SignInView,
                    UserViewSet,
                    ProductViewSet,
                    TagViewSet,
                    ProductImagesViewSet,
                    OrderViewSet,
                    ReviewViewSet,
                    BasketViewSet,
                    SignInAPIView)

app_name = 'frontend'

routers = DefaultRouter()
routers.register('about', UserViewSet)
routers.register('catalog', ProductViewSet)
routers.register('orders', OrderViewSet)
routers.register('review', ReviewViewSet)
routers.register('basket', BasketViewSet)
routers.register('tags', TagViewSet)


urlpatterns = [
    path('', TemplateView.as_view(template_name="frontend/index.html")),
    path('api/', include(routers.urls)),
    path('about/', TemplateView.as_view(template_name="frontend/about.html"), name='about'),
    path('account/', TemplateView.as_view(template_name="frontend/account.html")),
    path('cart/', TemplateView.as_view(template_name="frontend/cart.html")),
    path('catalog/', TemplateView.as_view(template_name="frontend/catalog.html")),
    path('catalog/<int:id>/', TemplateView.as_view(template_name="frontend/catalog.html")),
    path('history-order/', TemplateView.as_view(template_name="frontend/historyorder.html")),
    path('order-detail/<int:id>/', TemplateView.as_view(template_name="frontend/oneorder.html")),
    path('orders/<int:id>/', TemplateView.as_view(template_name="frontend/order.html")),
    path('payment/<int:id>/', TemplateView.as_view(template_name="frontend/payment.html")),
    path('payment-someone/', TemplateView.as_view(template_name="frontend/paymentsomeone.html")),
    path('product/<int:id>/', TemplateView.as_view(template_name="frontend/product.html")),
    path('profile/', TemplateView.as_view(template_name="frontend/profile.html")),
    path('progress-payment/', TemplateView.as_view(template_name="frontend/progressPayment.html")),
    path('sale/', TemplateView.as_view(template_name="frontend/sale.html")),
    path('sign-in/', SignInView.as_view(), name='signIn'),
    path('api/sign-in/', SignInAPIView.as_view(), name='signIn'),
    path('sign-up/', TemplateView.as_view(template_name="frontend/signUp.html")),
]
